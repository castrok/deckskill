import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {MatDialogModule} from "@angular/material/dialog";
import {Route, RouterModule} from "@angular/router";

const routes: Route[] = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'timeline',
    },

    {
        path: 'timeline',
        loadChildren: () => import('./unlogged/timeline/timeline.module').then(m => m.TimelineModule)
    }
]

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        MatDialogModule,
        RouterModule.forRoot(routes)
    ],
    providers: [
        {
            provide: LOCALE_ID,
            useValue: 'pt-PT'
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
