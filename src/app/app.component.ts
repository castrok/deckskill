import {Component} from '@angular/core';

@Component({
    selector: 'tml-root',
    template: `
        <nav></nav>
        <router-outlet></router-outlet>
    `,
    styles: [
        `nav {
            top: 0;
            min-height: 3rem;
            background-color: white;
            position: fixed;
            z-index: 1;
            width: 100%;
        }
        `
    ]
})
export class AppComponent {
}
