import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'tml-confirmation',
  templateUrl: './confirmation.component.html',
})
export class ConfirmationComponent {

  constructor(
      public dialogRef: MatDialogRef<any>,
      @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }

}
