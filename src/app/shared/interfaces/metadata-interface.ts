export default interface MetadataInterface {
    created_at?: Date | string
    updated_at?: Date
    deleted_at?: Date
}
