import MetadataInterface from "./metadata-interface";
import {AuthorInterface} from "./author-interface";

export interface PostInterface extends MetadataInterface {
    id: string
    content: string
    author: AuthorInterface
}

