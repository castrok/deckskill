import MetadataInterface from "./metadata-interface";

export interface AuthorInterface extends MetadataInterface {
    id: string
    name: string
    photo: string
    username: string
}
