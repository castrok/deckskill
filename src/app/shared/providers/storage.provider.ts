import {BehaviorSubject} from "rxjs";
import * as CryptoJS from "crypto-js";
import {PostInterface} from "../interfaces/post-interface";
import {Injectable} from "@angular/core";
@Injectable({
    providedIn: 'root'
})
export default class storageProvider {
    private _localstorage: any
    private localstorage = new BehaviorSubject(this.getLocalItem)
    private readonly key: string = "5OIOLcazUKQ7DOy0Df1pLV7GMJYH7uiKnEJ8B0qVAsIonFMOph"

    protected storeLocalItem(key: string, data: any): void {
        this.localstorage.next(data)
        localStorage.setItem(key, JSON.stringify(data))
    }

    protected getLocalItem(key: string): any {

        const localstorageData = localStorage.getItem(key)
        if (key && localstorageData) {
            this._localstorage = JSON.parse(localstorageData)
            return this._localstorage
        }
    }

    protected destroyLocalItem(key: string): void {
        localStorage.removeItem(key)
    }

    encriptNdStoreLocalStorage(key: string, object: any): void {
        const encripted = CryptoJS.AES.encrypt(JSON.stringify(object), this.key).toString()
        this.storeLocalItem(key, atob(encripted))
    }

    decriptNdGetLocalStorage(key: string): PostInterface[] | [] {
        const localstorageData = this.getLocalItem(key)

        if (localstorageData) {
            this._localstorage = CryptoJS.AES.decrypt(btoa(localstorageData), this.key).toString(CryptoJS.enc.Utf8)
            this._localstorage = JSON.parse(this._localstorage)
            return this._localstorage
        }
        return []
    }
}
