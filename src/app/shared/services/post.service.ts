import {Injectable} from '@angular/core';
import {PostInterface} from "../interfaces/post-interface";
import storageProvider from "../providers/storage.provider";

@Injectable({
    providedIn: 'root'
})
export class PostService {

    constructor(private storagePrivider: storageProvider) {
    }

    index(): PostInterface[] {
        return this.storagePrivider.decriptNdGetLocalStorage('timeline')
    }

    store(newPost: PostInterface): void {
        const storage: PostInterface[] = this.storagePrivider.decriptNdGetLocalStorage('timeline')
        storage.push(newPost)
        this.storagePrivider.encriptNdStoreLocalStorage('timeline', storage)
    }

    destroy(postUUID: string): boolean {
        const storage = this.index()
        const index: number = storage.findIndex(post => post.id === postUUID)

        if (index === -1)
            return false

        storage.splice(index, 1)
        this.storagePrivider.encriptNdStoreLocalStorage('timeline', storage)
        return true
    }
}
