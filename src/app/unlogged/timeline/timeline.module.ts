import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TimelineComponent} from './timeline.component';
import {RouterModule} from "@angular/router";
import {PostModule} from "../post/post.module";
import {FormModule} from "../form/form.module";

@NgModule({
    declarations: [
        TimelineComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                pathMatch: 'full',
                component: TimelineComponent
            }
        ]),
        PostModule,
        FormModule,
    ]
})
export class TimelineModule {
}
