import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PostService} from "../../shared/services/post.service";
import {PostInterface} from "../../shared/interfaces/post-interface";
import {v4 as uuidv4} from 'uuid';
import {AuthorInterface} from "../../shared/interfaces/author-interface";
import {formatDate} from "@angular/common";
import {MatDialog} from "@angular/material/dialog";
import {ConfirmationComponent} from "../../shared/components/confirmation/confirmation.component";
import {WhiteSpacesValidator} from "../../shared/validators/white-spaces.validator";

@Component({
    selector: 'tml-timeline',
    templateUrl: './timeline.component.html',
    styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent {

    event: any;

    constructor(
    ) {
    }

}
