import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostComponent } from './post.component';
import {ConfirmationModule} from "../../shared/components/confirmation/confirmation.module";


@NgModule({
    declarations: [
        PostComponent
    ],
    exports: [
        PostComponent
    ],
    imports: [
        CommonModule,
        ConfirmationModule,
    ]
})
export class PostModule { }
