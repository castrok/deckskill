import {Component, Input, OnInit} from '@angular/core';
import {PostService} from "../../shared/services/post.service";
import {PostInterface} from "../../shared/interfaces/post-interface";
import {ConfirmationComponent} from "../../shared/components/confirmation/confirmation.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
    selector: 'tml-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
    @Input()
    event: any

    ngOnChanges({event: {currentValue}}: any): void {
        if (currentValue)
            this.index()
    }

    posts: PostInterface[] = []

    constructor(
        private postService: PostService,
        private dialog: MatDialog,
    ) {
    }

    ngOnInit(): void {
        this.index()
    }

    index(): void {
        this.posts = this.postService.index().reverse()
        this.posts.map(post => post.created_at = this.normalizeCreatedAt(post.created_at))
    }

    delete(post: PostInterface): void {
        this.dialog.open(ConfirmationComponent, {
            id: 'destroy-confirmation-window'
        }).afterClosed().subscribe(
            response => {
                if (response === 'confirm') {
                    this.postService.destroy(post.id)
                    this.index()
                }
            }
        )

    }

    normalizeCreatedAt(date: any): string {
        const created_at: any = new Date(date)
        const now: any = new Date

        const elapsed = (now - created_at) / 1000;
        const seconds = Math.round(elapsed);

        return (seconds <= 60) ? ` - ${String(seconds)} s` : created_at.toLocaleString().slice(0, 17)
    }

}
