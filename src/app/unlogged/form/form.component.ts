import {Component, EventEmitter, Output} from '@angular/core';
import {AuthorInterface} from "../../shared/interfaces/author-interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WhiteSpacesValidator} from "../../shared/validators/white-spaces.validator";
import {PostInterface} from "../../shared/interfaces/post-interface";
import {PostService} from "../../shared/services/post.service";
import {v4 as uuidv4} from "uuid";

@Component({
    selector: 'tml-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss']
})
export class FormComponent {
    @Output()
    event: EventEmitter<PostInterface> = new EventEmitter<PostInterface>()
    author: AuthorInterface = {
        id: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
        name: 'John Doe',
        photo: 'assets/icons/john-256x256.png',
        username: 'johnDoe2023',
    }
    formGroup: FormGroup = this.formBuilder.group({
        content: [
            "",
            {
                validators: [Validators.required, Validators.maxLength(130), WhiteSpacesValidator()],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
        author: [
            this.author,
            {
                validators: [Validators.required],
                asyncValidators: [],
                updateOn: 'change',
            },
        ],
    })

    getFormGroupEntry(element: string) {
        return this.formGroup.get(element)
    }

    constructor(
        private formBuilder: FormBuilder,
        private postService: PostService,
    ) {
    }

    save(): void {
        if (this.formGroup.valid) {
            const author: AuthorInterface = this.getFormGroupEntry('author')?.value
            const content: string = this.getFormGroupEntry('content')?.value

            const post: PostInterface = {
                id: uuidv4(),
                created_at: new Date,
                author: author,
                content: content
            }
            this.postService.store(post)
            this.formGroup.get('content')?.patchValue(null)
            this.event.emit(post)
        }
    }

    preventTyping({code}: any): void {
        const inputElement = this.getFormGroupEntry('content')
        const inputValue = inputElement?.value
        const inputSize: number = inputValue?.length

        const bypassKeys = [
            'ArrowLeft',
            'ArrowRight',
            'Enter',
            'Tab',
            'ShiftRight',
            'ShiftLeft',
            'ControlLeft',
            'ControlRight',
        ]

        if (inputSize >= 130 && bypassKeys.indexOf(code) === -1)
            inputElement?.patchValue(inputValue.slice(0, 129))
    }
}



